package com.example.registrationretrofitapi

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import com.google.gson.GsonBuilder
import com.google.gson.JsonObject
import kotlinx.android.synthetic.main.activity_main.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainActivity : AppCompatActivity(),AdapterView.OnItemSelectedListener {

    val builder = GsonBuilder()
    val gson = builder.serializeNulls().create()
    private lateinit var countryList : Array<CountryData>
    private lateinit var stateList : Array<StateData>
    private lateinit var cityList : Array<CityData>
    private lateinit var cntryId :String
    private lateinit var stateId:String
    private lateinit var cityId:String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        spinnerCountry.onItemSelectedListener = this
        spinnerState.onItemSelectedListener = this
        spinnerCity.onItemSelectedListener = this

        countryApi()

        registrationButton.setOnClickListener {
            var firstName = editTextTextFirstName.text.toString()
            var lastName = editTextTextLastName.text.toString()
            var phone = editTextTextPhoneNumber.text.toString()
            var email = editTextTextPersonEmail.text.toString()
            var password = editTextTextPassword.text.toString()
            var confirmPass = editTextTextConfirmPassword.text.toString()
            var userType =1



            registrationApi(firstName,lastName,phone,email,password,cntryId,stateId,cityId,userType)

            Toast.makeText(this,cityId,Toast.LENGTH_SHORT).show()

        }
    }

    private fun registrationApi(firstName: String, lastName: String, phone: String, email: String, password: String, cntryId: String, stateId: String, cityId: String, userType: Int) {

        RetrofitObject.instance.regList(firstName,lastName,phone,email,password,cntryId,stateId,cityId, userType)
            .enqueue(object : Callback<JsonObject>{
                override fun onResponse(call: Call<JsonObject>, response: Response<JsonObject>) {

                    when {
                        response.code() == 400 -> {
                            val loginBase = gson.fromJson(response.errorBody()?.charStream(), Error::class.java)
                            Toast.makeText(applicationContext, loginBase.message, Toast.LENGTH_SHORT).show()
                        }
                        response.code() == 200 -> {
                            val loginBase = gson.fromJson(response.body().toString(), RegistrationResponse::class.java)
                            Toast.makeText(applicationContext, loginBase.message, Toast.LENGTH_SHORT).show()
                        }
                        else -> {
                            Toast.makeText(this@MainActivity, "Something went wrong", Toast.LENGTH_SHORT).show()
                        }

                    }
                }
                override fun onFailure(call: Call<JsonObject>, t: Throwable) {

                }

            })


    }
    private fun countryApi() {


        RetrofitObject.instance.countryList()
            .enqueue(object : Callback<JsonObject> {
                override fun onResponse(call: Call<JsonObject>, response: Response<JsonObject>) {

                    when{
                        response.code() == 200 -> {
                            CountrySpinnerLoad(response)
                        }

                        response.code() == 400 -> {
                            val res = gson.fromJson(response.errorBody()?.charStream(), Error::class.java)
                        }

                    }

                }

                override fun onFailure(call: Call<JsonObject>, t: Throwable) {
                    Toast.makeText(applicationContext, "Some error", Toast.LENGTH_SHORT).show()
                }

            })




    }

    private fun StateSpinnerLoad(response: Response<JsonObject>) {

        val res = gson.fromJson(response.body().toString(), StateResponse::class.java)
        stateList = res.data.toTypedArray()
        var state = arrayOfNulls<String>(stateList.size)

        for (i in stateList.indices) {
            state[i] = stateList[i].state_name
        }
        val adapter =  ArrayAdapter(this@MainActivity, android.R.layout.simple_spinner_item,state)
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerState.adapter = adapter
    }

    private fun CountrySpinnerLoad(response: Response<JsonObject>) {
        val res = gson.fromJson(response.body().toString(), CountryResponse::class.java)
        countryList = res.data.toTypedArray()
        var country = arrayOfNulls<String>(countryList.size)


        for (i in countryList.indices) {
            country[i] = countryList[i].country_name

        }

        val adapter =  ArrayAdapter(this@MainActivity, android.R.layout.simple_spinner_item,country)
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerCountry.adapter = adapter


    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {

        when(parent?.id) {
            R.id.spinnerCountry -> {
                cntryId = countryList.get(position).country_id

                Toast.makeText(this,cntryId,Toast.LENGTH_SHORT).show()


                RetrofitObject.instance.stateList(cntryId)
                    .enqueue(object : Callback<JsonObject>{
                        override fun onResponse(call: Call<JsonObject>, response: Response<JsonObject>) {

                            when{

                                response.code() == 200 -> {

                                    StateSpinnerLoad(response)
                                }

                                response.code() == 400 -> {
                                    val res = gson.fromJson(response.errorBody()?.charStream(), Error::class.java)
                                }


                            }
                        }

                        override fun onFailure(call: Call<JsonObject>, t: Throwable) {
                            Toast.makeText(applicationContext, "Some error",Toast.LENGTH_SHORT).show()
                        }

                    })

            }

            R.id.spinnerState -> {
                stateId = stateList.get(position).state_id
                Toast.makeText(this,stateId,Toast.LENGTH_SHORT).show()

                RetrofitObject.instance.cityList(stateId)
                    .enqueue(object : Callback<JsonObject>{
                        override fun onResponse(call: Call<JsonObject>, response: Response<JsonObject>) {

                            when{

                                response.code() == 200 -> {
                                    CitySpinnerLoad(response)
                                }

                                response.code() == 400 -> {
                                    val res = gson.fromJson(response.errorBody()?.charStream(), Error::class.java)
                                }


                            }

                        }

                        override fun onFailure(call: Call<JsonObject>, t: Throwable) {

                        }

                    })


            }
            R.id.spinnerCity -> {
                cityId = cityList.get(position).city_id
            }

        }

    }

    private fun CitySpinnerLoad(response: Response<JsonObject>) {

        val res = gson.fromJson(response.body().toString(), CityResponse::class.java)
        cityList = res.data.toTypedArray()
        var city = arrayOfNulls<String>(cityList.size)


        for (i in cityList.indices) {
            city[i] = cityList[i].city_name

        }

        val adapter =  ArrayAdapter(this@MainActivity, android.R.layout.simple_spinner_item,city)
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerCity.adapter = adapter

    }

    override fun onNothingSelected(parent: AdapterView<*>?) {
        TODO("Not yet implemented")
    }


}