package com.example.registrationretrofitapi

data class StateResponse (
    val data: List<StateData>,
    val message: String,
    val status: String
)