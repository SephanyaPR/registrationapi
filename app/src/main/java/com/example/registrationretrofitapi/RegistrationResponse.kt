package com.example.registrationretrofitapi

data class RegistrationResponse (
    val data: RegistrationData,
    val message: String,
    val status: String
)