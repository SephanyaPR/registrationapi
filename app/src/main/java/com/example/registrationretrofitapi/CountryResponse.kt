package com.example.registrationretrofitapi

data class CountryResponse(
    val data: List<CountryData>,
    val message: String,
    val status: String
)



data class Error(val status: String?,val message: String?)