package com.example.registrationretrofitapi

data class CityData (
    val city_id: String,
    val city_name: String
)