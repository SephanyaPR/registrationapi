package com.example.registrationretrofitapi

data class StateData (
    val state_id: String,
    val state_name: String
)